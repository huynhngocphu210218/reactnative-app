import moment from 'moment';
import firestore from '@react-native-firebase/firestore';
export const resetObject = obj => {
  const rs = {};
  Object.keys(obj).forEach(key => {
    if (obj[key] instanceof Array) rs[key] = [];
    else rs[key] = '';
  });
  return rs;
};
export const toDateString = date => {
  return moment(date).format('YYYY-MM-DD');
};

export const getAllDocsFromCollection = async (collectionName, where) => {
  try {
    const collection = firestore().collection(collectionName);
    let querySnapshot = [];
    if (where) {
      querySnapshot = await collection
        .where(where.key, where.logic, where.value)
        .get();
    } else {
      querySnapshot = await collection.get();
    }

    const documents = [];

    querySnapshot.forEach(documentSnapshot => {
      const docId = documentSnapshot.id;
      const docData = documentSnapshot.data();
      documents.push({id: docId, ...docData});
    });

    return documents;
  } catch (error) {
    console.error('Error getting documents: ', error);
    return [];
  }
};
