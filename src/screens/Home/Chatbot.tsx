import React, {useState, useRef} from 'react';
import {
  View,
  StatusBar,
  FlatList,
  Keyboard,
  Text,
  ScrollView,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import {
  useTheme,
  ActivityIndicator,
  Caption,
  Modal,
  Portal,
  Paragraph,
} from 'react-native-paper';
import GoogleAI from '../../utils/GoogleAI';
import {ListAccordionGroupContext} from 'react-native-paper/lib/typescript/components/List/ListAccordionGroup';
import SafeAreaView from 'react-native-safe-area-view';
import AweSomeIcon from 'react-native-vector-icons/FontAwesome';
import {useNavigation} from '@react-navigation/native';

const Chatbot = () => {
  const navigation = useNavigation();
  const [text, setText] = useState('');
  const [data, setData] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  const onChat = async () => {
    console.log('kkkk text', text);
    if (!text) return;
    const _txt = `${text}`;
    console.log('kkk _txt', _txt);
    setData(txtData => {
      return [...txtData, {content: _txt, type: 'user'}];
    });
    Keyboard.dismiss();
    setIsLoading(true);
    setText('');
    const _rs = await GoogleAI.genMessage(_txt);
    const _msg = _rs?.candidates?.[0]?.content;
    console.log('kkk _msg', _msg);
    if (_msg) {
      setData(txtData => {
        return [...txtData, {content: _msg, type: 'ai'}];
      });
    } else {
      setData(txtData => {
        return [
          ...txtData,
          {content: 'Error..no data found', type: 'ai', isError: true},
        ];
      });
    }
    setIsLoading(false);
  };

  const _renderItem = ({item}) => {
    return (
      <Paragraph
        style={{
          backgroundColor: item.isError
            ? '#FF0000'
            : item.type === 'user'
            ? '#0066FF'
            : '#E0FFFF',
          color: item.type === 'ai' ? 'black' : null,
          fontSize: 20,
          alignSelf: item.type === 'user' ? 'flex-end' : 'flex-start',
          paddingHorizontal: 4,
          marginHorizontal: 4,
          margin: 10,
          padding: 15,
          borderRadius: 10,
        }}>
        {item.content}
      </Paragraph>
    );
  };

  return (
    <View style={{flex: 1}}>
      {/* header */}
      <View
        style={{
          // flex: 1,
          padding: 10,
          backgroundColor: '#6AB7E3',
          flexDirection: 'row',
          justifyContent: 'center',
          alignContent: 'center',
        }}>
        <Text style={{marginLeft: 20, fontSize: 25, color: 'white'}}>
          CHAT AI
        </Text>
      </View>
      {/* body */}

      <View style={{flex: 1}}>
        <FlatList
          data={data}
          renderItem={_renderItem}
          keyExtractor={(item, index) => `${index}`}
        />
      </View>
      {/* footer */}
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <View
          style={{
            // marginTop: 150,
            // flex: 2,
            backgroundColor: '#E8E8E8',
            borderRadius: 15,
            borderWidth: 1,
            flexDirection: 'row',
            width: '80%',
            // justifyContent: 'center',
            // alignItems: 'center',
            marginBottom: 15,
            marginLeft: 10,
          }}>
          <View>
            <TextInput
              style={{fontSize: 18}}
              placeholder={'Q&A'}
              multiline={true}
              value={text}
              onChangeText={text => setText(text)}
            />
          </View>
        </View>
        <View>
          <TouchableOpacity style={{margin: 10}} onPress={onChat}>
            <AweSomeIcon
              style={{marginBottom: 10}}
              name="send"
              size={20}
              color={'#0000FF'}
            />
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

export default Chatbot;
