import React from 'react';
import {View} from 'react-native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import TabScreen from './TabScreen';
import Chatbot from './Chatbot';
import Registermajors from './Registerevent';
import Settings from './Settings';
import AwesomeIcon from 'react-native-vector-icons/FontAwesome';

const Tab = createBottomTabNavigator();

const HomeScreen = () => {
  return (
    <Tab.Navigator
      screenOptions={{headerShown: false, tabBarActiveTintColor: 'blue'}}>
      <Tab.Screen
        options={{
          tabBarIcon: ({color}) => (
            <AwesomeIcon name="home" color={color} size={30} />
          ),
        }}
        name="Home"
        component={TabScreen}
      />
      <Tab.Screen
        options={{
          tabBarIcon: ({color}) => (
            <AwesomeIcon name="wechat" color={color} size={30} />
          ),
        }}
        name="Chat"
        component={Chatbot}
      />
      <Tab.Screen
        options={{
          tabBarIcon: ({color}) => (
            <AwesomeIcon name="plus-square-o" color={color} size={30} />
          ),
        }}
        name="Registermajors"
        component={Registermajors}
      />
      <Tab.Screen
        options={{
          tabBarIcon: ({color}) => (
            <AwesomeIcon name="cog" color={color} size={30} />
          ),
        }}
        name="Settings"
        component={Settings}
      />
    </Tab.Navigator>
  );
};
export default HomeScreen;
