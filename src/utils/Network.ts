
class Network {
  constructor() {}

  async post(url, data) {
    // Create the request headers
    const headers = {
      'Content-Type': 'application/json', // Set the content type to JSON
    };

    // Create the request options
    const requestOptions = {
      method: 'POST', // Use the POST method
      headers: headers,
      body: JSON.stringify(data), // Convert the data to a JSON string
    };
    try {
      const response = await fetch(url, requestOptions);

      if (response.ok) {
        // Request was successful, handle the response
        const responseData = await response.json(); // Parse the response body as JSON
        console.log('Network post:: res:', responseData);
        return responseData;
      } else {
        // Handle errors
        console.error('Request failed with status:', response.status);
      }
      return;
    } catch (error) {
      // Handle network or other errors
      console.error('Request failed with error:', error);
      return;
    }
  }
}

export default new Network();
