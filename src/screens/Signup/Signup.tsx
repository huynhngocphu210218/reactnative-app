import React, {useState} from 'react';
import {
  View,
  Text,
  SafeAreaView,
  TouchableOpacity,
  TextInput,
  Button,
  // TextBase,
  // Modal,
  // Image,
  // StyleSheet,
  ScrollView,
  Alert,
  FlatList,
} from 'react-native';
import AwesomeIcom from 'react-native-vector-icons/FontAwesome';
import SysModal from '../../compoments/sys_modal';
import {useNavigation} from '@react-navigation/native';
// import DatePickers from 'react-native-date-picker';
import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';
const AppSignup = () => {
  const navigation = useNavigation();
  // user input username && password
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  // const [confirmpassword, setConfirmpassword] = useState('');
  const [email, setEmail] = useState('');
  const [fullname, setFullname] = useState('');
  const [phone, setPhone] = useState('');
  const [address, setAddress] = useState('');
  const [showmodal, setShowModal] = useState(false);
  // const [address, setAddress] = useState('');
  // hide modal
  const onHideModal = () => {
    setShowModal(false);
  };

  // user click login button
  // const onClickLogin = () => {
  //   if (username.length == 0 || password.length == 0) {
  //     setErrorMessage('Please input login information.');
  //     setShowModal(true);
  //     return;
  //   }
  // };
  // user name, password auth
  const signUpClick = () => {
    auth()
      .createUserWithEmailAndPassword(email, password)
      .then(() => {
        firestore()
          .collection('Users')
          .add({
            username: username,
            fullname: fullname,
            phone: phone,
            address: address,
          })
          .then(() => {
            navigation.goBack();
            console.log('User added!');
          });
        console.log('User account created & signed in!');
      })
      .catch(error => {
        if (error.code === 'auth/email-already-in-use') {
          console.log('That email address is already in use!');
        }

        if (error.code === 'auth/invalid-email') {
          console.log('That email address is invalid!');
        }

        console.error(error);
      });
  };

  return (
    <SafeAreaView style={{backgroundColor: 'blue', flex: 1}}>
      <View
        style={{
          backgroundColor: 'white',
          margin: 10,
          flex: 1,
          borderRadius: 10,
        }}>
        <View style={{flex: 1, marginVertical: 20}}>
          {/* Header */}
          <View
            style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text
              style={{
                color: 'black',
                fontWeight: 'bold',
                fontSize: 30,
              }}>
              SIGN UP
            </Text>
          </View>
          {/* Body */}
          <View style={{flex: 10}}>
            <ScrollView style={{margin: 30}}>
              {/* username */}
              <View>
                <Text style={{color: 'black'}}>Username</Text>
                <View
                  style={{
                    flexDirection: 'row',
                    borderBottomColor: 'grey',
                    borderBottomWidth: 1,
                  }}>
                  <View
                    style={{
                      justifyContent: 'center',
                      alignItems: 'center',
                      padding: 15,
                    }}>
                    <AwesomeIcom name="user" size={15} />
                  </View>
                  <View>
                    <TextInput
                      style={{}}
                      placeholder={'Username Enter...'}
                      value={username}
                      onChangeText={username => setUsername(username)}
                    />
                  </View>
                </View>
              </View>
              {/* email */}
              <View
                style={{
                  marginTop: 20,
                }}>
                <Text style={{color: 'black'}}>Email</Text>
                <View
                  style={{
                    flexDirection: 'row',
                    borderBottomColor: 'grey',
                    borderBottomWidth: 1,
                  }}>
                  <View
                    style={{
                      justifyContent: 'center',
                      alignItems: 'center',
                      padding: 15,
                    }}>
                    <AwesomeIcom name="envelope" size={15} />
                  </View>
                  <View>
                    <TextInput
                      style={{}}
                      // secureTextEntry={true}
                      placeholder={'Email Enter...'}
                      value={email}
                      onChangeText={email => setEmail(email)}
                    />
                  </View>
                </View>
              </View>
              {/* password */}
              <View
                style={{
                  marginTop: 20,
                }}>
                <Text style={{color: 'black'}}>Password</Text>
                <View
                  style={{
                    flexDirection: 'row',
                    borderBottomColor: 'grey',
                    borderBottomWidth: 1,
                  }}>
                  <View
                    style={{
                      justifyContent: 'center',
                      alignItems: 'center',
                      padding: 15,
                    }}>
                    <AwesomeIcom name="lock" size={15} />
                  </View>
                  <View>
                    <TextInput
                      style={{}}
                      secureTextEntry={true}
                      placeholder={'Password Enter...'}
                      value={password}
                      onChangeText={password => setPassword(password)}
                    />
                  </View>
                </View>
              </View>
              {/* fullname */}
              <View
                style={{
                  marginTop: 20,
                }}>
                <Text style={{color: 'black'}}>FullName</Text>
                <View
                  style={{
                    flexDirection: 'row',
                    borderBottomColor: 'grey',
                    borderBottomWidth: 1,
                  }}>
                  <View
                    style={{
                      justifyContent: 'center',
                      alignItems: 'center',
                      padding: 15,
                    }}>
                    <AwesomeIcom name="users" size={15} />
                  </View>
                  <View>
                    <TextInput
                      style={{}}
                      placeholder={'FullName Enter...'}
                      value={fullname}
                      onChangeText={fullname => setFullname(fullname)}
                    />
                  </View>
                </View>
              </View>
              {/* phone */}
              <View
                style={{
                  marginTop: 20,
                }}>
                <Text style={{color: 'black'}}>Phone</Text>
                <View
                  style={{
                    flexDirection: 'row',
                    borderBottomColor: 'grey',
                    borderBottomWidth: 1,
                  }}>
                  <View
                    style={{
                      justifyContent: 'center',
                      alignItems: 'center',
                      padding: 15,
                    }}>
                    <AwesomeIcom name="phone" size={15} />
                  </View>
                  <View>
                    <TextInput
                      style={{}}
                      placeholder={'Phone Enter...'}
                      value={phone}
                      onChangeText={phone => setPhone(phone)}
                    />
                  </View>
                </View>
              </View>
              {/* address */}
              <View
                style={{
                  marginTop: 20,
                }}>
                <Text style={{color: 'black'}}>Address</Text>
                <View
                  style={{
                    flexDirection: 'row',
                    borderBottomColor: 'grey',
                    borderBottomWidth: 1,
                  }}>
                  <View
                    style={{
                      justifyContent: 'center',
                      alignItems: 'center',
                      padding: 15,
                    }}>
                    <AwesomeIcom name="address-card" size={15} />
                  </View>
                  <View>
                    <TextInput
                      style={{}}
                      placeholder={'Address Enter...'}
                      value={address}
                      onChangeText={address => setAddress(address)}
                    />
                  </View>
                </View>
              </View>
              {/* login button */}
            </ScrollView>
            <View
              style={{
                marginVertical: 20,
                margin: 50,
                borderRadius: 20,
              }}>
              <Button title="SIGN UP" onPress={signUpClick}></Button>
            </View>
          </View>
          {/* Footer */}
          <View
            style={{flex: 2, justifyContent: 'center', alignItems: 'center'}}>
            <TouchableOpacity
              style={{padding: 20}}
              onPress={() => navigation.goBack()}>
              <Text style={{color: 'black', fontWeight: '500'}}>BACK</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </SafeAreaView>
  );
};

export default AppSignup;
