import React, {useState, useEffect, useContext} from 'react';
import {
  View,
  Text,
  SafeAreaView,
  TouchableOpacity,
  TextInput,
  Button,
  // TextBase,
  // Modal,
  // Image,
  // StyleSheet,
  Alert,
} from 'react-native';
import AwesomeIcom from 'react-native-vector-icons/FontAwesome';
// import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
// import LinearGradient from 'react-native-linear-gradient';
import {useNavigation} from '@react-navigation/native';
import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';
import {observer} from 'mobx-react';
import {useStore} from '../../store/index';
import {GoogleSignin} from '@react-native-google-signin/google-signin';

const AppLogin = observer(() => {
  const navigation = useNavigation();
  const {userStore} = useStore();
  // user input username && password
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [showmodal, setShowModal] = useState(false);
  // hide modal
  const onHideModal = () => {
    setShowModal(false);
  };
  const onChangeEmail = value => {
    setEmail(value);
    console.log('kkkk', value);
  };

  const onChangePassword = value => {
    setPassword(value);
    console.log('kkkk', value);
  };
  async function onGoogleButtonPress() {
    // Check if your device supports Google Play
    await GoogleSignin.hasPlayServices({showPlayServicesUpdateDialog: true});
    // Get the users ID token
    const {idToken} = await GoogleSignin.signIn();
    console.log('kkktoken', idToken);

    // Create a Google credential with the token
    const googleCredential = auth.GoogleAuthProvider.credential(idToken);
    console.log('kkk googleCredential', googleCredential);
    // Sign-in the user with the credential
    const _user = await auth().signInWithCredential(googleCredential);
    console.log(' signed in google!');
    console.log('kkkk _user', _user);
    navigation.navigate('Home');
    const _usersid = await firestore()
      .collection('Users')
      .doc(_user.user.uid)
      .get();
    console.log('kkk _userid', _usersid);
    console.log('kkk userStore', userStore);
    if (_usersid.exists) {
      userStore.setUser(_usersid.data());
      console.log('kkkk userStore', userStore?.user);
    } else {
      firestore()
        .collection('Users')
        .doc(_user.user.uid)
        .set({
          role: 'user',
        })
        .then(() => {
          console.log('User added!');
        });
      console.log('User account created & signed in! google!!');
    }
  }
  const onClickLogin = async () => {
    if (!email.trim() || !password.trim()) {
      Alert.alert('Warning', 'Requires entering Email or Password', [
        {text: 'OK', onPress: () => console.log('OK Pressed')},
      ]);
      return;
    } else {
      const _user = await auth().signInWithEmailAndPassword(email, password);
      console.log('kkkk _user', _user);
      navigation.navigate('Home');
      const _usersid = await firestore()
        .collection('Users')
        .doc(_user.user.uid)
        .get();
      console.log('kkk _userid', _usersid);
      console.log('kkk userStore', userStore);
      if (_usersid.exists) {
        userStore.setUser(_usersid.data());
        console.log('kkkk userStore', userStore?.user);
      } else {
        firestore()
          .collection('Users')
          .doc(_user.user.uid)
          .set({
            role: 'user',
          })
          .then(() => {
            console.log('User added!');
          });
        console.log('User account created & signed in!');
      }
    }

    // firestore()
    //   .collection('Users')
    //   .add({
    //     name: 'Ada Lovelace',
    //     age: 30,
    //   })
    //   .then(() => {
    //     console.log('User added!');
    //   });

    // firestore()
    //   .collection('Users')
    //   .get()
    // .then(querySnapshot => {
    //   console.log('Total users: ', querySnapshot.size);
    //   querySnapshot.forEach(documentSnapshot => {
    //     console.log(
    //       'User ID: ',
    //       documentSnapshot.id,
    //       documentSnapshot.data(),
    //     );
    //   });
    // });
    // const users = await firestore().collection('Users').get();
    // users.forEach(documentSnapshot => {
    //   console.log('User ID: ', documentSnapshot.id, documentSnapshot.data());
    // });
    // if (email.length == 0 || password.length == 0) {
    //   setErrorMessage('Please input login information.');
    //   setShowModal(true);
    // }
    // console.log('kkkkk', rs);
    // return;
  };
  // const onCameraClick = async () => {
  //   const result = await launchCamera({mediaType: 'photo', saveToPhotos: true});
  //   console.log('in hinh anh', result.assets[0].uri);
  //   const img = result.assets[0].uri;
  //   setImage(img);
  // };
  return (
    <SafeAreaView style={{backgroundColor: 'blue', flex: 1}}>
      {/* {image ? (
        <Image style={styles.tinyLogo} source={{uri: image}}></Image>
      ) : null}
      {imagelibrary ? (
        <Image style={styles.tinyLogo} source={{uri: imagelibrary}}></Image>
      ) : null} */}
      <View
        style={{
          backgroundColor: 'white',
          margin: 10,
          flex: 1,
          borderRadius: 10,
        }}>
        <View style={{flex: 1, marginVertical: 20}}>
          {/* Header */}
          <View
            style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text
              style={{
                color: 'black',
                fontWeight: 'bold',
                fontSize: 30,
              }}>
              LOGIN
            </Text>
          </View>
          {/* Body */}
          <View style={{flex: 6}}>
            <View style={{margin: 30}}>
              {/* email */}
              <View>
                <Text style={{color: 'black'}}>Email</Text>
                <View
                  style={{
                    flexDirection: 'row',
                    borderBottomColor: 'grey',
                    borderBottomWidth: 1,
                  }}>
                  <View
                    style={{
                      justifyContent: 'center',
                      alignItems: 'center',
                      padding: 15,
                    }}>
                    <AwesomeIcom name="user" size={15} />
                  </View>
                  <View>
                    <TextInput
                      style={{}}
                      placeholder={'Email...'}
                      value={email}
                      onChangeText={onChangeEmail}
                    />
                  </View>
                </View>
              </View>
              {/* password */}
              <View
                style={{
                  marginTop: 20,
                }}>
                <Text style={{color: 'black'}}>Password</Text>
                <View
                  style={{
                    flexDirection: 'row',
                    borderBottomColor: 'grey',
                    borderBottomWidth: 1,
                  }}>
                  <View
                    style={{
                      justifyContent: 'center',
                      alignItems: 'center',
                      padding: 15,
                    }}>
                    <AwesomeIcom name="lock" size={15} />
                  </View>
                  <View>
                    <TextInput
                      style={{}}
                      secureTextEntry={true}
                      placeholder={'Password...'}
                      value={password}
                      onChangeText={onChangePassword}
                    />
                  </View>
                </View>
              </View>
              {/* forgot password */}
              <View
                style={{
                  marginTop: 10,
                  alignItems: 'flex-end',
                }}>
                <Text
                  style={{
                    color: 'black',
                  }}>
                  Forgot Password?
                </Text>
              </View>
              {/* login button */}
              <View
                style={{
                  marginVertical: 20,
                  // borderRadius: 15,
                  // padding: 80,
                }}>
                <Button title="LOGIN" onPress={onClickLogin}></Button>
              </View>
            </View>
            <View
              style={{
                marginVertical: 20,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text>Or Sign Up Using</Text>
              <View
                style={{
                  marginTop: 20,
                  flexDirection: 'row',
                }}>
                {/* fb */}
                <TouchableOpacity
                  style={{
                    width: 40,
                    height: 40,
                    padding: 10,
                    backgroundColor: '#3b5998',
                    borderRadius: 50,
                    justifyContent: 'center',
                    alignItems: 'center',
                    marginRight: 5,
                  }}>
                  <AwesomeIcom name="facebook" size={20} color={'white'} />
                </TouchableOpacity>
                {/* tw */}
                <TouchableOpacity
                  style={{
                    width: 40,
                    height: 40,
                    padding: 10,
                    backgroundColor: '#00aced',
                    borderRadius: 50,
                    justifyContent: 'center',
                    alignItems: 'center',
                    marginRight: 5,
                  }}>
                  <AwesomeIcom name="twitter" size={20} color={'#ffffff'} />
                </TouchableOpacity>
                {/* gg */}
                <TouchableOpacity
                  style={{
                    width: 40,
                    height: 40,
                    padding: 10,
                    backgroundColor: '#DB4437',
                    borderRadius: 50,
                    justifyContent: 'center',
                    alignItems: 'center',
                    marginRight: 5,
                  }}
                  onPress={() =>
                    onGoogleButtonPress().then(() =>
                      console.log('Signed in with Google!'),
                    )
                  }>
                  <AwesomeIcom name="google" size={20} color={'#ffffff'} />
                </TouchableOpacity>
              </View>
            </View>
          </View>
          {/* Footer */}
          <View
            style={{flex: 2, justifyContent: 'center', alignItems: 'center'}}>
            <Text>Or Sign Up Using</Text>
            <TouchableOpacity
              style={{padding: 20}}
              onPress={() => navigation.navigate('Signup')}>
              <Text style={{color: 'black', fontWeight: '500'}}>SIGN UP</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </SafeAreaView>
  );
});

export default AppLogin;
