import React, {useState, useEffect} from 'react';
import {View, Text, Dimensions, ScrollView} from 'react-native';
import {getAllDocsFromCollection} from '../../utils/Common';

import {
  LineChart,
  BarChart,
  PieChart,
  ProgressChart,
  ContributionGraph,
  StackedBarChart,
} from 'react-native-chart-kit';
import {ScreenContainer} from 'react-native-screens';
import {Title} from 'react-native-paper';

const Charts = () => {
  const [data, setData] = useState([]);
  const [charts, setCharts] = useState([]);

  useEffect(() => {
    getData();
  }, []);

  const getData = async () => {
    const _events = await getAllDocsFromCollection('Detail_RegisterEvent');
    for (let index = 0; index < _events.length; index++) {
      const _event = _events[index];
      const _registered = await getAllDocsFromCollection('Register', {
        key: 'IDDetail',
        logic: '==',
        value: _event.id,
      });
      console.log('kkkkk _registered', _registered);
      const _joined = await getAllDocsFromCollection('Joined', {
        key: 'IDRegister',
        logic: '==',
        value: _event.id,
      });
      console.log('kkkkk _joined', _joined);
      const chart = [
        {
          name: 'Đã đăng ký',
          population: _registered?.length,
          color: 'rgba(131, 167, 234, 1)',
          legendFontColor: '#7F7F7F',
          legendFontSize: 15,
        },
        {
          name: 'Đã tham gia',
          population: _joined?.length,
          color: '#F00',
          legendFontColor: '#7F7F7F',
          legendFontSize: 15,
        },
      ];
      setCharts(prev => [...prev, chart]);
    }
    setData(_events);
    console.log('kkk table _events', _events);
  };
  //   const data = [
  //     {
  //       name: 'Seoul',
  //       population: 21500000,
  //       color: 'rgba(131, 167, 234, 1)',
  //       legendFontColor: '#7F7F7F',
  //       legendFontSize: 15,
  //     },
  //   ];
  //   const Charts = () => {

  const screenWidth = Dimensions.get('window').width;

  return (
    <ScrollView>
      <View
        style={{
          // flex: 1,
          padding: 10,
          backgroundColor: '#6AB7E3',
          flexDirection: 'row',
          justifyContent: 'center',
          alignContent: 'center',
        }}>
        <Text style={{fontSize: 25, color: 'white'}}>STATISTICAL</Text>
      </View>
      {charts.map((c, idx) => (
        <ScrollView style={{margin: 10}} key={idx}>
          <Text style={{fontSize: 18, fontWeight: 'bold'}}>
            {data[idx]?.Title}
          </Text>
          <PieChart
            data={c}
            width={screenWidth}
            height={220}
            chartConfig={{
              backgroundColor: '#e26a00',
              backgroundGradientFrom: '#fb8c00',
              backgroundGradientTo: '#ffa726',
              decimalPlaces: 2, // optional, defaults to 2dp
              color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
              labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
              style: {
                borderRadius: 16,
              },
              propsForDots: {
                r: '6',
                strokeWidth: '2',
                stroke: '#ffa726',
              },
            }}
            accessor={'population'}
            backgroundColor={'transparent'}
            // paddingLeft={'15'}
            center={[5, 20]}
            absolute
          />
        </ScrollView>
      ))}
    </ScrollView>
  );
};

export default Charts;
