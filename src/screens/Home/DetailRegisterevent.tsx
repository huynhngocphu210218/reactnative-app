import {
  View,
  Text,
  ScrollView,
  TextInput,
  Button,
  TouchableOpacity,
  Alert,
  Modal,
  FlatList,
} from 'react-native';
import React from 'react';
import {Dropdown} from 'react-native-element-dropdown';
import {useState, useEffect} from 'react';
import {getAllDocsFromCollection} from '../../utils/Common';
import firestore from '@react-native-firebase/firestore';
import AwesomeIcom from 'react-native-vector-icons/FontAwesome';
import Title from './Title/Subtitle/Title';
import {useRoute} from '@react-navigation/native';
import Subtitle from './Title/Subtitle/Subtitle';
import {useStore} from '../../store/index';
import {useNavigation} from '@react-navigation/native';

const DetailRegisterevent = () => {
  // const navigation = useNavigation();
  const {userStore} = useStore();
  const navigation = useNavigation();
  console.log('kkk userStore', userStore?.user);
  const [title, setTitle] = useState('');
  const [content, setContent] = useState('');
  const [fullname, setFullname] = useState('');
  const [email, setEmail] = useState('');
  const [link, setLink] = useState('');
  const [registernew, setRegisternew] = useState('');
  const [modalVisible, setModalVisible] = useState(false);
  const [modalRegisterU, setModalRegisterU] = useState(false);
  const [isEdit, setIsEdit] = useState(false);
  const [data, setData] = useState([]);
  const [dataregister, setDataRegister] = useState([]);
  const [item, setItem] = useState<any>({});
  const route = useRoute();
  const [fullnameuser, setFullnameuser] = useState('');
  const [number, setNumber] = useState('');
  const [emailuser, setEmailuser] = useState('');
  const [detailR, setDetailR] = useState('');
  const [dataR, setDataR] = useState([]);

  // Access the params object from the route
  const myParam: any = route.params;
  console.log('kkkkk myParam', myParam);

  useEffect(() => {
    getData();
  }, []);

  const getData = async () => {
    const _registerevent = await getAllDocsFromCollection('RegisterEvent');
    const _detailregisterevent = await getAllDocsFromCollection(
      'Detail_RegisterEvent',
    );
    const _register = await getAllDocsFromCollection('Register');
    setDataR(_register);
    setDataRegister(_registerevent);
    setData(
      _detailregisterevent.filter((r: any) => r.IDRegister === myParam.id),
    );
    console.log('kkk table _registerevent', _registerevent);
    console.log('kkk table _detailregisterevent', _detailregisterevent);
    console.log('kkk table _register', _register);
  };

  // const _registerevent = _item => {
  //   firestore().collection('RegisterEvent').doc(_item.id);
  //   console.log('kkk _registerevent', _item);
  //   // setItem(_item);
  //   setRegisternew(_item.id);
  // };
  const onAdd = () => {
    setIsEdit(false);
    setModalVisible(true);
  };
  const onRegister = () => {
    // setIsEdit(false);
    setModalRegisterU(true);
  };
  const onEdit = _item => {
    setModalVisible(true);
    setIsEdit(true);
    // setIsEdit(true);
    setItem(_item);
  };
  const onDelete = _item => {
    firestore()
      .collection('Detail_RegisterEvent')
      .doc(_item.id)
      .delete()
      .then(() => {
        console.log('Detail_RegisterEvent deleted!');
      });
    // setModalDelete(false);
  };
  const _register = _item => {
    firestore().collection('RegisterEvent').doc(_item.id);
    console.log('kkk _register', _item);
    // setItem(_item);
    setRegisternew(_item.id);
  };
  const _registerdetail = _item => {
    firestore().collection('Detail_RegisterEvent').doc(_item.id);
    console.log('kkk _registerdetail', _item);
    // setItem(_item);
    setDetailR(_item.id);
  };

  const onUserregister = async _item => {
    const existingExam = await firestore()
      .collection('Register')
      .where('Email', '==', emailuser)
      .get();
    try {
      if (!fullnameuser.trim() || (!emailuser.trim() && !detailR.trim())) {
        Alert.alert('Warning', 'Requires entering FullName or Email and ID', [
          {text: 'OK', onPress: () => console.log('OK Pressed')},
        ]);
        return;
      } else if (!existingExam.empty) {
        Alert.alert(
          'Warning',
          'Exam with the same name already exists. Not adding.',
          [{text: 'OK', onPress: () => console.log('OK Pressed')}],
        );
        return;
      } else {
        await firestore()
          .collection('Register')
          .add({
            FullName: fullnameuser,
            Numbers: number,
            Email: emailuser,
            IDDetail: detailR,
          })
          .then(() => {
            console.log('Register added!');
            navigation.navigate('ListRegister');
          });
        setModalRegisterU(false);
      }
    } catch (error) {
      console.log('kkk erorr _onUserregister', error);
    }
  };

  const onFinish = async () => {
    try {
      if (
        !fullname.trim() ||
        !title.trim() ||
        !email.trim() ||
        !link.trim() ||
        (!content.trim() && !registernew.trim())
      ) {
        Alert.alert('Warning', 'Requires entering FullName or Email and ID', [
          {text: 'OK', onPress: () => console.log('OK Pressed')},
        ]);
        return;
      } else {
        if (isEdit) {
          await firestore()
            .collection('Detail_RegisterEvent')
            .doc(item.id)
            .update({
              Title: title,
              Content: content,
              FullName: fullname,
              Email: email,
              Link: link,
              IDRegister: registernew,
            });
          setModalVisible(false);
        } else {
          await firestore()
            .collection('Detail_RegisterEvent')
            .add({
              Title: title,
              Content: content,
              FullName: fullname,
              Email: email,
              Link: link,
              IDRegister: registernew,
            })
            .then(() => {
              console.log('DetailRegisterEvent added!');
            });
          setModalVisible(false);
        }
      }
    } catch (error) {
      console.error();
      ('kkkkk _error update or add');
    }
  };

  const renderItem = ({item}) => {
    return (
      <ScrollView>
        <View
          style={{
            marginHorizontal: 1,
            marginVertical: 2,
            // elevation: 1,
            borderRadius: 10,
            borderColor: 'black',
            // padding: 10,
            // backgroundColor: '#fff',
            // borderWidth: 0.5,
            // flex: 1,
          }}>
          {/* modal register user */}
          <Modal
            animationType="slide"
            transparent={true}
            visible={modalRegisterU}
            onRequestClose={() => {
              Alert.alert('closed.');
              setModalRegisterU(!modalRegisterU);
            }}>
            <View
              style={{
                // margin: 10,
                marginLeft: 20,
                marginRight: 20,
                marginTop: 30,
                backgroundColor: 'white',
                borderRadius: 20,
                padding: 25,
                alignItems: 'center',
                // shadowColor: '#000',
                // shadowOffset: {
                //   width: 2,
                //   height: 0,
                // },
                borderWidth: 0.5,
              }}>
              {/* Fullname*/}
              <View
                style={{
                  //   margin: 20,
                  //   backgroundColor: 'pink',
                  width: 250,
                  height: 60,
                }}>
                <Text style={{color: 'black', fontSize: 15}}>Full Name</Text>
                <View
                  style={{
                    flexDirection: 'row',
                    borderBottomColor: 'gray',
                    borderBottomWidth: 1,
                  }}>
                  <View>
                    <TextInput
                      style={{}}
                      // secureTextEntry={true}
                      placeholder={'Full Name Enter...'}
                      value={fullnameuser}
                      onChangeText={fullnameuser =>
                        setFullnameuser(fullnameuser)
                      }
                    />
                  </View>
                </View>
              </View>
              {/* Email */}
              <View
                style={{
                  //   margin: 20,
                  //   backgroundColor: 'pink',
                  width: 250,
                  height: 60,
                }}>
                <Text style={{color: 'black', fontSize: 15}}>Email</Text>
                <View
                  style={{
                    flexDirection: 'row',
                    borderBottomColor: 'gray',
                    borderBottomWidth: 1,
                  }}>
                  <View>
                    <TextInput
                      style={{}}
                      // secureTextEntry={true}
                      placeholder={'Email Enter...'}
                      value={emailuser}
                      onChangeText={emailuser => setEmailuser(emailuser)}
                    />
                  </View>
                </View>
              </View>
              {/* Numbers */}
              <View
                style={{
                  //   margin: 20,
                  //   backgroundColor: 'pink',
                  width: 250,
                  height: 60,
                }}>
                <Text style={{color: 'black', fontSize: 15}}>Numbers</Text>
                <View
                  style={{
                    flexDirection: 'row',
                    borderBottomColor: 'gray',
                    borderBottomWidth: 1,
                  }}>
                  <View>
                    <TextInput
                      style={{}}
                      // secureTextEntry={true}
                      placeholder={'Number Enter...'}
                      value={number}
                      onChangeText={number => setNumber(number)}
                    />
                  </View>
                </View>
              </View>
              {/* IDDeatil */}
              <Dropdown
                style={{
                  // marginBottom: 10,
                  width: '100%',
                  height: 45,
                  borderBottomColor: 'white',
                  borderRadius: 7,
                  borderWidth: 1,
                  // flexDirection: 'row',
                  marginRight: 10,
                  margin: 10,
                  marginTop: 50,
                }}
                placeholderStyle={{fontSize: 16}}
                // selectedTextStyle={{fontSize: 16}}
                // inputSearchStyle={styles.inputSearchStyle}
                data={data}
                // search
                // maxHeight={50}
                labelField="Title"
                valueField="id"
                placeholder=""
                onChange={_registerdetail}
                value={detailR}
                // onChangeText={yearnew => setYearnew(yearnew)}
              />
              <View style={{margin: 10, marginTop: 80, borderRadius: 30}}>
                <Button
                  title={`${isEdit ? 'Edit' : 'đăng ký'} `}
                  onPress={onUserregister}
                />
              </View>
              <View
                style={{
                  marginTop: 10,
                  //   backgroundColor: 'red',
                  //   padding: 24,
                  width: 80,
                  height: 40,
                  //   justifyContent: 'center',
                  //   alignItems: 'center',
                  //   borderRadius: 15,
                  //   borderWidth: 0.5,
                }}>
                <Button
                  color="red"
                  title="close"
                  onPress={() => setModalRegisterU(!modalRegisterU)}
                />
              </View>
            </View>
          </Modal>
          <View style={{padding: 5, flexDirection: 'row'}}>
            <View style={{}}>
              <Text
                style={{
                  fontSize: 20,
                  fontWeight: '500',
                  // alignContent: 'center',
                }}>
                {item.Title}
              </Text>
              {/* <Subtitle>{item.Drescription}</Subtitle> */}
              <View style={{flexDirection: 'row'}}>
                <View
                  style={{
                    // position: 'absolute',
                    marginRight: 10,
                    width: 30,
                    height: 30,
                    borderRadius: 999,
                    padding: 24,
                    backgroundColor: 'gray',
                    marginBottom: 15,
                    marginTop: 15,
                  }}
                />
                <View style={{marginTop: 15}}>
                  <Text style={{fontSize: 18, fontWeight: '500'}}>
                    {item.FullName}
                  </Text>
                  <Text style={{fontSize: 12, fontWeight: '300'}}>
                    {item.Email}
                  </Text>
                </View>
              </View>
              <Text
                style={{
                  color: 'black',
                  fontSize: 16,
                  fontWeight: '400',
                  marginBottom: 10,
                }}>
                {item.Content}
              </Text>
              <Text style={{fontSize: 16, fontWeight: '500'}}>
                {item.FullName}
              </Text>
              <Text style={{color: 'black', fontSize: 16, fontWeight: '500'}}>
                Link:
                {item.Link}
              </Text>
              <View style={{}}>
                {userStore?.user?.role === 'admin' && (
                  <TouchableOpacity
                    style={{
                      backgroundColor: '#FF0000',
                      width: 28,
                      height: 28,
                      borderRadius: 9999,
                      position: 'absolute',
                      right: -4,
                      bottom: -8,
                      justifyContent: 'center',
                      alignItems: 'center',
                      // margin: 10,
                      marginBottom: 120,
                      marginRight: 5,
                      // marginLeft: 100,
                    }}
                    onPress={() => onDelete(item)}>
                    <AwesomeIcom name="trash-o" size={15} color="#fff" />
                  </TouchableOpacity>
                )}
                {userStore?.user?.role === 'admin' && (
                  <TouchableOpacity
                    style={{
                      backgroundColor: '#007bff',
                      width: 28,
                      height: 28,
                      borderRadius: 9999,
                      position: 'absolute',
                      right: -4,
                      bottom: -8,
                      justifyContent: 'center',
                      alignItems: 'center',
                      // margin: 10,
                      marginBottom: 120,
                      marginRight: 35,
                    }}
                    onPress={() => onEdit(item)}>
                    <AwesomeIcom name="pencil" size={15} color="#fff" />
                  </TouchableOpacity>
                )}
              </View>
              <View
                style={{
                  // flexDirection: 'row',
                  // marginLeft: 220,
                  margin: 100,
                  // backgroundColor: 'pink',
                }}>
                <View>
                  <Button title="đăng ký" onPress={onRegister} />
                </View>
              </View>
            </View>
          </View>
        </View>
      </ScrollView>
    );
  };

  return (
    <View>
      {/* header */}
      <View
        style={{
          // flex: 1,
          padding: 10,
          backgroundColor: '#6AB7E3',
          flexDirection: 'row',
          justifyContent: 'center',
          alignContent: 'center',
        }}>
        {userStore?.user?.role === 'admin' && (
          <TouchableOpacity onPress={onAdd}>
            <AwesomeIcom
              style={{marginLeft: 300}}
              name="plus"
              size={30}
              color={'white'}
            />
          </TouchableOpacity>
        )}
      </View>
      {/* body */}
      <View>
        <ScrollView>
          {/* modal add, edit */}
          <Modal
            animationType="slide"
            transparent={true}
            visible={modalVisible}
            onRequestClose={() => {
              Alert.alert('closed.');
              setModalVisible(!modalVisible);
            }}>
            <View
              style={{
                // margin: 10,
                marginLeft: 20,
                marginRight: 20,
                marginTop: 30,
                backgroundColor: 'white',
                borderRadius: 20,
                padding: 25,
                alignItems: 'center',
                // shadowColor: '#000',
                // shadowOffset: {
                //   width: 2,
                //   height: 0,
                // },
                borderWidth: 0.5,
              }}>
              {/* Title */}
              <View
                style={{
                  //   margin: 20,
                  //   backgroundColor: 'pink',
                  width: 250,
                  height: 60,
                }}>
                <Text style={{color: 'black', fontSize: 15}}>Title</Text>
                <View
                  style={{
                    flexDirection: 'row',
                    borderBottomColor: 'gray',
                    borderBottomWidth: 1,
                  }}>
                  <View>
                    <TextInput
                      style={{}}
                      // secureTextEntry={true}
                      placeholder={'Title Enter...'}
                      value={title}
                      onChangeText={title => setTitle(title)}
                    />
                  </View>
                </View>
              </View>
              {/* Fullname*/}
              <View
                style={{
                  //   margin: 20,
                  //   backgroundColor: 'pink',
                  width: 250,
                  height: 60,
                }}>
                <Text style={{color: 'black', fontSize: 15}}>Full Name</Text>
                <View
                  style={{
                    flexDirection: 'row',
                    borderBottomColor: 'gray',
                    borderBottomWidth: 1,
                  }}>
                  <View>
                    <TextInput
                      style={{}}
                      // secureTextEntry={true}
                      placeholder={'Full Name Enter...'}
                      value={fullname}
                      onChangeText={fullname => setFullname(fullname)}
                    />
                  </View>
                </View>
              </View>
              {/* Email */}
              <View
                style={{
                  //   margin: 20,
                  //   backgroundColor: 'pink',
                  width: 250,
                  height: 60,
                }}>
                <Text style={{color: 'black', fontSize: 15}}>Email</Text>
                <View
                  style={{
                    flexDirection: 'row',
                    borderBottomColor: 'gray',
                    borderBottomWidth: 1,
                  }}>
                  <View>
                    <TextInput
                      style={{}}
                      // secureTextEntry={true}
                      placeholder={'Email Enter...'}
                      value={email}
                      onChangeText={email => setEmail(email)}
                    />
                  </View>
                </View>
              </View>
              {/* Link */}
              <View
                style={{
                  //   margin: 20,
                  //   backgroundColor: 'pink',
                  width: 250,
                  height: 60,
                }}>
                <Text style={{color: 'black', fontSize: 15}}>Link</Text>
                <View
                  style={{
                    flexDirection: 'row',
                    borderBottomColor: 'gray',
                    borderBottomWidth: 1,
                  }}>
                  <View>
                    <TextInput
                      style={{}}
                      // secureTextEntry={true}
                      placeholder={'Link Enter...'}
                      value={link}
                      onChangeText={link => setLink(link)}
                    />
                  </View>
                </View>
              </View>
              {/* Content */}
              <View
                style={{
                  //   margin: 20,
                  //   backgroundColor: 'pink',
                  width: 250,
                  height: 60,
                }}>
                <Text style={{color: 'black', fontSize: 15}}>Content</Text>
                <View
                  style={{
                    // flexDirection: 'row',
                    borderBottomColor: 'gray',
                    borderWidth: 1,
                    // height: '200%',
                    borderRadius: 10,
                    // margin: 10,
                    width: 250,
                  }}>
                  <View>
                    <TextInput
                      style={{
                        height: 150,
                        textAlignVertical: 'top',
                        justifyContent: 'flex-start',
                      }}
                      multiline={true}
                      numberOfLines={30}
                      placeholder={'Content Enter...'}
                      value={content}
                      onChangeText={content => setContent(content)}
                    />
                  </View>
                </View>
              </View>
              {/* idregister */}
              <Dropdown
                style={{
                  // marginBottom: 10,
                  width: '100%',
                  height: 45,
                  borderBottomColor: 'white',
                  borderRadius: 7,
                  borderWidth: 1,
                  // flexDirection: 'row',
                  marginRight: 10,
                  margin: 10,
                  marginTop: 120,
                }}
                placeholderStyle={{fontSize: 16}}
                // selectedTextStyle={{fontSize: 16}}
                // inputSearchStyle={styles.inputSearchStyle}
                data={dataregister}
                // search
                // maxHeight={50}
                labelField="Title"
                valueField="id"
                placeholder=""
                onChange={_register}
                value={registernew}
                // onChangeText={yearnew => setYearnew(yearnew)}
              />
              <View style={{margin: 10, marginTop: 80, borderRadius: 30}}>
                <Button
                  title={`${isEdit ? 'Edit' : 'Add'} `}
                  onPress={onFinish}
                />
              </View>
              <View
                style={{
                  marginTop: 10,
                  //   backgroundColor: 'red',
                  //   padding: 24,
                  width: 80,
                  height: 40,
                  //   justifyContent: 'center',
                  //   alignItems: 'center',
                  //   borderRadius: 15,
                  //   borderWidth: 0.5,
                }}>
                <Button
                  color="red"
                  title="close"
                  onPress={() => setModalVisible(!modalVisible)}
                />
              </View>
            </View>
          </Modal>
          {/* click modal add */}
        </ScrollView>
        <ScrollView style={{margin: 10}}>
          <FlatList
            data={data}
            //   keyExtractor={item => {
            //     item.id.toString();
            //   }}
            renderItem={renderItem}
          />
        </ScrollView>
      </View>
    </View>
  );
};

export default DetailRegisterevent;
