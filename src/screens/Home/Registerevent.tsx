import {
  View,
  Text,
  ScrollView,
  TextInput,
  Button,
  TouchableOpacity,
  Alert,
  Modal,
  FlatList,
} from 'react-native';
import React from 'react';
import {Dropdown} from 'react-native-element-dropdown';
import {useState, useEffect} from 'react';
import {getAllDocsFromCollection} from '../../utils/Common';
import firestore from '@react-native-firebase/firestore';
import AwesomeIcom from 'react-native-vector-icons/FontAwesome';
import Title from './Title/Subtitle/Title';
import Subtitle from './Title/Subtitle/Subtitle';
import {useNavigation} from '@react-navigation/native';
import Toast from 'react-native-toast-message';
import {useStore} from '../../store/index';

const Registerevent = () => {
  const navigation = useNavigation();
  const {userStore} = useStore();
  console.log('kkk userStore', userStore?.user);
  // const [value, setValue] = useState(null);
  const [title, setTitle] = useState('');
  const [description, setDescription] = useState('');
  const [modalVisible, setModalVisible] = useState(false);
  const [modalVisibleDetail, setModalVisibleDetai] = useState(false);
  const [modalVisibleRegister, setModalVisibleRegister] = useState(false);
  const [isEdit, setIsEdit] = useState(false);
  const [data, setData] = useState([]);
  const [detaildata, setDetaildata] = useState([]);
  const [registerdetail, setRegisterdetail] = useState('');
  const [item, setItem] = useState<any>({});

  useEffect(() => {
    getData();
  }, []);

  const getData = async () => {
    const _registerevent = await getAllDocsFromCollection('RegisterEvent');
    const _detailregisterevent = await getAllDocsFromCollection(
      'Detail_RegisterEvent',
    );
    setDetaildata(_detailregisterevent);
    setData(_registerevent);
    console.log('kkk table _registerevent', _registerevent);
    console.log('kkk table _detailregisterevent', _detailregisterevent);
  };

  const onAdd = () => {
    setIsEdit(false);
    setModalVisible(true);
  };
  const onEdit = _item => {
    // const _user = users.find(k => k.id === id);
    setModalVisible(true);
    setIsEdit(true);
    setItem(_item);

    // setData();
  };
  const onDetail = _item => {
    // setModalVisibleDetai(true);
    navigation.navigate('DetailRegisterevent', _item);
  };
  const onRegister = async () => {
    setModalVisibleRegister(true);
  };

  const _registerdetail = _item => {
    firestore().collection('Detail_RegisterEvent').doc(_item.id);
    console.log('kkk _registerdetail', _item);
    // setItem(_item);
    setRegisterdetail(_item.id);
  };

  const onFinish = async () => {
    try {
      if (isEdit) {
        await firestore().collection('RegisterEvent').doc(item.id).update({
          Title: title,
          Drescription: description,
        });
        setModalVisible(false);
      } else {
        await firestore()
          .collection('RegisterEvent')
          .add({
            Title: title,
            Drescription: description,
          })
          .then(() => {
            console.log('RegisterEvent added!');
          });
        setModalVisible(false);
      }
    } catch (error) {
      console.log('kkkkk _error update or add');
    }
  };

  const renderItem = ({item}) => {
    // navigation.navigate('DetailRegisterevent');
    // console.log('kkk detaildata', item, detaildata);
    // const _Detailid = detaildata.find(k => k.IDRegister == item.id);
    // console.log('kkkk _detail', _Detailid);
    return (
      <ScrollView
        style={{
          marginHorizontal: 1,
          marginVertical: 5,
          // elevation: 1,
          borderRadius: 10,
          borderColor: 'black',
          // padding: 10,
          backgroundColor: '#fff',
          borderWidth: 0.5,
        }}>
        <TouchableOpacity onPress={() => onDetail(item)}>
          <View style={{padding: 5}}>
            <View>
              <Title>{item.Title}</Title>
              <Subtitle>{item.Drescription}</Subtitle>
            </View>
            <View>
              {userStore?.user?.role === 'admin' && (
                <TouchableOpacity
                  style={{
                    backgroundColor: '#007bff',
                    width: 28,
                    height: 28,
                    borderRadius: 9999,
                    position: 'absolute',
                    right: -4,
                    bottom: -8,
                    justifyContent: 'center',
                    alignItems: 'center',
                    margin: 4,
                    marginBottom: 45,
                  }}
                  onPress={() => onEdit(item)}>
                  <AwesomeIcom name="pencil" size={15} color="#fff" />
                </TouchableOpacity>
              )}
            </View>
          </View>
        </TouchableOpacity>
      </ScrollView>
    );
  };

  return (
    <View>
      {/* header */}
      <View
        style={{
          // flex: 1,
          padding: 10,
          backgroundColor: '#6AB7E3',
          flexDirection: 'row',
          justifyContent: 'center',
          alignContent: 'center',
        }}>
        <Text
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            fontSize: 25,
            color: 'white',
          }}>
          REGISTER
        </Text>
        {userStore?.user?.role === 'admin' && (
          <TouchableOpacity
            style={{
              // Left: 100,
              marginLeft: 20,
              justifyContent: 'flex-end',
              // backgroundColor: 'white',
            }}
            onPress={onAdd}>
            <AwesomeIcom style={{}} name="plus" size={30} color={'white'} />
          </TouchableOpacity>
        )}
      </View>
      {/* body */}
      <View>
        <View>
          {/* modal add, edit */}
          <Modal
            animationType="slide"
            transparent={true}
            visible={modalVisible}
            onRequestClose={() => {
              Alert.alert('closed.');
              setModalVisible(!modalVisible);
            }}>
            <View
              style={{
                // margin: 10,
                marginLeft: 20,
                marginRight: 20,
                marginTop: 160,
                backgroundColor: 'white',
                borderRadius: 20,
                padding: 25,
                alignItems: 'center',
                // shadowColor: '#000',
                // shadowOffset: {
                //   width: 2,
                //   height: 0,
                // },
                borderWidth: 0.5,
              }}>
              {/* Title */}
              <View
                style={{
                  //   margin: 20,
                  //   backgroundColor: 'pink',
                  width: 250,
                  height: 60,
                }}>
                <Text style={{color: 'black', fontSize: 15}}>Title</Text>
                <View
                  style={{
                    flexDirection: 'row',
                    borderBottomColor: 'gray',
                    borderBottomWidth: 1,
                  }}>
                  <View>
                    <TextInput
                      style={{}}
                      // secureTextEntry={true}
                      placeholder={'Title Enter...'}
                      value={title}
                      onChangeText={title => setTitle(title)}
                    />
                  </View>
                </View>
              </View>
              {/* Description */}
              <View
                style={{
                  margin: 20,
                  //   backgroundColor: 'pink',
                  width: 250,
                  height: 60,
                }}>
                <Text style={{color: 'black', fontSize: 15}}>Description</Text>
                <View
                  style={{
                    // flexDirection: 'row',
                    borderBottomColor: 'gray',
                    borderWidth: 1,
                    // height: '150',
                    borderRadius: 10,
                    // margin: 10,
                    width: 250,
                  }}>
                  <View>
                    <TextInput
                      style={{
                        height: 150,
                        textAlignVertical: 'top',
                        justifyContent: 'flex-start',
                      }}
                      multiline={true}
                      numberOfLines={30}
                      // secureTextEntry={true}
                      placeholder={'Description Enter...'}
                      value={description}
                      onChangeText={description => setDescription(description)}
                    />
                  </View>
                </View>
              </View>
              <View style={{margin: 10, marginTop: 120, borderRadius: 30}}>
                <Button
                  title={`${isEdit ? 'Edit' : 'Add'} `}
                  onPress={onFinish}
                />
              </View>
              <View
                style={{
                  marginTop: 10,
                  //   backgroundColor: 'red',
                  //   padding: 24,
                  width: 80,
                  height: 40,
                  //   justifyContent: 'center',
                  //   alignItems: 'center',
                  //   borderRadius: 15,
                  //   borderWidth: 0.5,
                }}>
                <Button
                  color="red"
                  title="close"
                  onPress={() => setModalVisible(!modalVisible)}
                />
              </View>
            </View>
          </Modal>
          {/* click modal add */}
        </View>
        <ScrollView style={{margin: 10}}>
          <FlatList
            data={data}
            //   keyExtractor={item => {
            //     item.id.toString();
            //   }}
            renderItem={renderItem}
          />
        </ScrollView>
      </View>
    </View>
  );
};

export default Registerevent;
