import {
  View,
  Text,
  ScrollView,
  TextInput,
  Button,
  TouchableOpacity,
  Alert,
  Modal,
  FlatList,
  Image,
  Platform,
} from 'react-native';
// import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import React from 'react';
import {useState, useEffect} from 'react';
import AwesomeIcom from 'react-native-vector-icons/FontAwesome';
import {getAllDocsFromCollection} from '../../utils/Common';
import {useNavigation} from '@react-navigation/native';
import {BottomTabView} from '@react-navigation/bottom-tabs';
import Title from './Title/Subtitle/Title';
import Subtitle from './Title/Subtitle/Subtitle';
import firestore from '@react-native-firebase/firestore';
import {utils} from '@react-native-firebase/app';
import storage from '@react-native-firebase/storage';
// import ImagePicker from 'react-native-image-crop-picker';
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
import {useStore} from '../../store/index';
import filter from 'lodash.filter';

const TabScreen = () => {
  const navigation = useNavigation();
  const {userStore} = useStore();
  console.log('kkk userStore', userStore?.user);
  const [modalVisible, setModalVisible] = useState(false);
  const [isEdit, setIsEdit] = useState(false);
  const [title, setTitle] = useState('');
  const [description, setDescription] = useState('');
  // const [item, setItem] = useState<any>({});
  const [data, setData] = useState([]);
  const [fulldata, setFullData] = useState([]);
  const [image, setImage] = useState(null);
  // const [filteredData, setFilteredData] = useState(data);
  const [query, setQuery] = useState('');
  // const [library, setLibrary] = useState(null);

  useEffect(() => {
    getData();
  }, []);

  const getData = async () => {
    const _event = await getAllDocsFromCollection('Event');
    for (let index = 0; index < _event.length; index++) {
      const _ev = _event[index];
      if (_ev.FileName) {
        const url = await storage().ref(_ev.FileName).getDownloadURL();
        _event[index]['Uri'] = url;
      }
    }
    setFullData(_event);
    setData(_event);
    console.log('kkk table _event', _event);
  };
  const onSearch = text => {
    setQuery(text);
    const formattedQuery = text.toLowerCase();
    const filterData = filter(fulldata, user => {
      return contains(user, formattedQuery);
    });
    setData(filterData);
  };
  const contains = ({Drescription, Title}, text) => {
    if (Drescription.includes(text) || Title.includes(text)) {
      return true;
    }
    return false;
  };
  const onAdd = () => {
    setIsEdit(false);
    setModalVisible(true);
  };

  const onDetailEvent = _item => {
    // setModalVisibleDetai(true);
    navigation.navigate('DetailEvent', _item);
  };

  const onFinish = async () => {
    try {
      const fileName = await saveImage();
      if (isEdit) {
        await firestore().collection('Event').doc(item.id).update({
          Title: title,
          Drescription: description,
        });
        setModalVisible(false);
      } else {
        await firestore()
          .collection('Event')
          .add({
            Title: title,
            Drescription: description,
            FileName: fileName,
          })
          .then(() => {
            console.log('Event added!');
          });
        setModalVisible(false);
      }
    } catch (error) {
      console.log('kkkkk _error update or add');
    }
  };

  const saveImage = async () => {
    if (!image) return;
    const timestamp = new Date().getTime();

    // Generate a random string (you can use a library for more randomness)
    const randomString = Math.random().toString(36).substring(2, 8);

    // Combine timestamp, random string, and file extension to create a unique file name
    const uniqueFileName = `${timestamp}_${randomString}.jpg`;
    const reference = storage().ref(uniqueFileName);
    // uploads file
    await reference.putFile(image);
    return uniqueFileName;
  };

  const onCameraClick = async () => {
    const result = await launchCamera({
      mediaType: 'photo',
      saveToPhotos: true,
    });
    console.log('in hinh anh', result.assets[0].uri);
    const img = result.assets[0].uri;
    setImage(img);
  };
  const onLibraryClick = async () => {
    const result = await launchImageLibrary({
      mediaType: 'photo',
      saveToPhotos: true,
    });
    console.log('in hinh anh thu vien', result.assets[0].uri);
    const img = result.assets[0].uri;
    setImage(img);
  };

  // const takeLibrary = () => {
  //   ImagePicker.openPicker({
  //     width: 800,
  //     height: 450,
  //     cropping: true,
  //   }).then(image => {
  //     console.log('kkk image', image);
  //     const imageUri = Platform.OS === 'ios' ? image.sourceURL : image.path;
  //     setImage(imageUri);
  //   });
  // };

  const   renderItem = ({item}) => {
    return (
      <TouchableOpacity onPress={() => onDetailEvent(item)}>
        <View
          style={{
            marginHorizontal: 1,
            marginVertical: 5,
            // elevation: 1,
            borderRadius: 10,
            borderColor: 'black',
            // padding: 10,
            backgroundColor: '#fff',
            borderWidth: 0.5,
          }}>
          <View style={{padding: 5}}>
            <View>
              {item.Uri && (
                <Image
                  source={{uri: item.Uri}}
                  style={{height: 200, width: 'auto'}}
                />
              )}
              <Title>{item.Title}</Title>
              <Subtitle>{item.Drescription}</Subtitle>
            </View>
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  return (
    <View
      style={{
        // backgroundColor: 'white',
        // margin: 10,
        flex: 1,
        borderRadius: 10,
      }}>
      {/* header */}
      <View
        style={{
          flexDirection: 'row',
          // flex: 0.5,
          padding: 10,
          // margin: 15,
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: '#6AB7E3',
          // borderRadius: 5,
        }}>
        <View
          style={{
            width: '85%',
            height: 40,
            backgroundColor: 'white',
            borderRadius: 15,
            flexDirection: 'row',
          }}>
          <TouchableOpacity
            style={{
              margin: 10,
            }}>
            <AwesomeIcom name="search" size={20} />
          </TouchableOpacity>
          <View>
            <TextInput
              placeholder="Sreach here..."
              value={query}
              onChangeText={text => onSearch(text)}
            />
          </View>
        </View>
        <View>
          {userStore?.user?.role === 'admin' && (
            <TouchableOpacity
              style={{
                margin: 10,
              }}
              onPress={onAdd}>
              <AwesomeIcom name="edit" size={30} />
            </TouchableOpacity>
          )}
        </View>
      </View>
      <View style={{flex: 1, marginVertical: 10}}>
        {/* body */}
        <ScrollView
          style={{
            flex: 5,
          }}>
          <View>
            {/* modal add, edit */}
            <Modal
              animationType="slide"
              transparent={true}
              visible={modalVisible}
              onRequestClose={() => {
                Alert.alert('closed.');
                setModalVisible(!modalVisible);
              }}>
              <View
                style={{
                  // margin: 10,
                  marginLeft: 20,
                  marginRight: 20,
                  marginTop: 20,
                  backgroundColor: 'white',
                  borderRadius: 20,
                  padding: 25,
                  alignItems: 'center',
                  // shadowColor: '#000',
                  // shadowOffset: {
                  //   width: 2,
                  //   height: 0,
                  // },
                  borderWidth: 0.5,
                }}>
                {/* Image */}
                {/* <View
                  style={{
                    padding: 10,
                    width: '70%',
                    height: 150,
                    borderRadius: 15,
                    borderWidth: 0.5,
                    margin: 10,
                  }}> */}
                {image ? (
                  <Image
                    style={{
                      padding: 10,
                      width: '100%',
                      height: 150,
                      borderRadius: 15,
                      borderWidth: 0.5,
                      // margin: 1,
                      // styles.tinyLogo,
                      // marginRight: 30,
                    }}
                    source={{uri: image}}
                    // resizeMode="cover"
                  />
                ) : null}

                {/* </View> */}
                <View style={{flexDirection: 'row'}}>
                  <TouchableOpacity
                    style={{
                      padding: 10,
                      borderRadius: 999,
                      borderWidth: 0.5,
                      backgroundColor: '#4682B4',
                      margin: 5,
                    }}
                    onPress={onCameraClick}>
                    <AwesomeIcom name="camera" size={20} color={'white'} />
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={{
                      padding: 10,
                      borderRadius: 999,
                      borderWidth: 0.5,
                      backgroundColor: '#FF0000',
                      margin: 5,
                    }}
                    onPress={onLibraryClick}>
                    <AwesomeIcom
                      name="file-image-o"
                      size={20}
                      color={'white'}
                    />
                  </TouchableOpacity>
                </View>
                {/* Title */}
                <View
                  style={{
                    //   margin: 20,
                    //   backgroundColor: 'pink',
                    width: 250,
                    height: 60,
                  }}>
                  <Text style={{color: 'black', fontSize: 15}}>Title</Text>
                  <View
                    style={{
                      flexDirection: 'row',
                      borderBottomColor: 'gray',
                      borderBottomWidth: 1,
                    }}>
                    <View>
                      <TextInput
                        style={{}}
                        // secureTextEntry={true}
                        placeholder={'Title Enter...'}
                        value={title}
                        onChangeText={title => setTitle(title)}
                      />
                    </View>
                  </View>
                </View>
                {/* Description */}
                <View
                  style={{
                    margin: 20,
                    //   backgroundColor: 'pink',
                    width: 250,
                    height: 60,
                  }}>
                  <Text style={{color: 'black', fontSize: 15}}>
                    Description
                  </Text>
                  <View
                    style={{
                      // flexDirection: 'row',
                      borderBottomColor: 'gray',
                      borderWidth: 1,
                      // height: '150',
                      borderRadius: 10,
                      // margin: 10,
                      width: 250,
                    }}>
                    <View>
                      <TextInput
                        style={{
                          height: 150,
                          textAlignVertical: 'top',
                          justifyContent: 'flex-start',
                        }}
                        multiline={true}
                        numberOfLines={30}
                        // secureTextEntry={true}
                        placeholder={'Description Enter...'}
                        value={description}
                        onChangeText={description =>
                          setDescription(description)
                        }
                      />
                    </View>
                  </View>
                </View>
                <View style={{margin: 10, marginTop: 120, borderRadius: 30}}>
                  <Button
                    title={`${isEdit ? 'Edit' : 'Add'} `}
                    onPress={onFinish}
                  />
                </View>
                <View
                  style={{
                    marginTop: 10,
                    //   backgroundColor: 'red',
                    //   padding: 24,
                    width: 80,
                    height: 40,
                    //   justifyContent: 'center',
                    //   alignItems: 'center',
                    //   borderRadius: 15,
                    //   borderWidth: 0.5,
                  }}>
                  <Button
                    color="red"
                    title="close"
                    onPress={() => setModalVisible(!modalVisible)}
                  />
                </View>
              </View>
            </Modal>
            {/* click modal add */}
          </View>

          {/* <Image
              style={{width: '100%', height: 200}}
              source={require('../../../assets/ttsv.png')}
            /> */}
          {/* <View style={{padding: 5}}>
              <Title>
                Giá trị cốt lõi: ĐỒNG THUẬN - TẬN TÂM - CHUẨN MỰC - SÁNG TẠO. 1.
                TRƯỜNG TRỌNG ĐIỂM QUỐC GIA Đại học Cần Thơ là cơ sở đào tạo đại
                học và sau đại học trọng điểm của Quốc gia, là 1 trong 3 trường
                của Việt Nam là thành viên chính thức của Mạng lưới các trường
                đại học Đông Nam Á (AUN-ASEAN University Network); xếp hạng thứ
                3 trong nước, thứ 59 khu vực Đông Nam Á (Webometrics).
              </Title>
              <Subtitle>
                Giá trị cốt lõi: ĐỒNG THUẬN - TẬN TÂM - CHUẨN MỰC - SÁNG TẠO. 1.
                TRƯỜNG TRỌNG ĐIỂM QUỐC GIA Đại học Cần Thơ là cơ sở đào tạo đại
                học và sau đại học trọng điểm của Quốc gia, là 1 trong 3 trường
                của Việt Nam là thành viên chính thức của Mạng lưới các trường
                đại học Đông Nam Á (AUN-ASEAN University Network); xếp hạng thứ
                3 trong nước, thứ 59 khu vực Đông Nam Á (Webometrics).
              </Subtitle>
            </View> */}
          <FlatList
            data={data}
            //   keyExtractor={item => {
            //     item.id.toString();
            //   }}
            renderItem={renderItem}
          />
        </ScrollView>
      </View>
      {/* footer */}
    </View>
  );
};

export default TabScreen;
