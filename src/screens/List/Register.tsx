import React, {useState, useEffect} from 'react';
import {
  Text,
  View,
  FlatList,
  TouchableOpacity,
  ScrollView,
  Linking,
  Modal,
  Button,
  Alert,
} from 'react-native';
import {getAllDocsFromCollection} from '../../utils/Common';
import firestore from '@react-native-firebase/firestore';
import {TextInput} from 'react-native-paper';
import AwesomeIcom from 'react-native-vector-icons/FontAwesome';
import {useStore} from '../../store/index';
import {Link} from '@react-navigation/native';
import filter from 'lodash.filter';

// import {Link} from '@react-navigation/native';

const ListRegister = () => {
  const {userStore} = useStore();
  console.log('kkk userStore', userStore?.user);
  const [data, setData] = useState([]);
  const [dataDetail, setDataDetail] = useState([]);
  const [detailR, setDetailR] = useState('');
  // const [modalVisible, setModalVisible] = useState(false);
  const [query, setQuery] = useState('');
  const [fulldata, setFullData] = useState([]);

  // const [status, setStatus] = useState('');

  useEffect(() => {
    getData();
  }, []);

  const getData = async () => {
    const register = await getAllDocsFromCollection('Register');
    const _events = await getAllDocsFromCollection('Detail_RegisterEvent');
    setData(register);
    setDataDetail(_events);
    setFullData(register);
    console.log('kkk table register', register);
    console.log('kkk table _events', _events);
  };

  const onSearch = text => {
    // setIsEdit(false);
    // setModalVisible(true);
    setQuery(text);
    const formattedQuery = text.toLowerCase();
    const filterData = filter(fulldata, user => {
      return contains(user, formattedQuery);
    });
    setData(filterData);
  };
  const contains = ({FullName, Email}, text) => {
    if (FullName.includes(text) || Email.includes(text)) {
      return true;
    }
    return false;
  };
  const onFinish = async (_item, link) => {
    // await firestore().collection('Detail_RegisterEvent').doc(_item.IDDetail);
    // console.log('kkk _registerdetail', _item);
    // // setItem(_item);
    // setDetailR(_item.IDDetail);
    // await firestore().collection('Register').doc(_item.id);
    // console.log('kkk _registeremail', _item);
    // // setItem(_item);
    // setEmailR(_item.Email);
    console.log('kkk id link', link);
    await Linking.openURL(link);
    await firestore()
      .collection('Joined')
      .add({
        Status: 'Đã tham gia',
        IDRegister: detailR,
      })
      .then(() => {
        console.log('Joined added!');
      });
  };
  const onDelete = _item => {
    firestore()
      .collection('Register')
      .doc(_item.id)
      .delete()
      .then(() => {
        console.log('Register deleted!');
      });
    // setModalDelete(false);
  };

  const renderItem = ({item}) => {
    // console.log('kkk datamajor', item, datamajor);
    const _detailregisterevent = dataDetail.find(k => k.id == item.IDDetail);
    // const _id = ;
    setDetailR(item.IDDetail);
    // const _sum = var toan=parseInt(item.Mon_toan)
    // console.log('kk _sum', _sum);
    console.log('kkk _detailregisterevent', _detailregisterevent);
    return (
      <ScrollView>
        <View
          style={{
            flexDirection: 'row',
            // justifyContent: 'space-between',
            marginHorizontal: 2,
            marginVertical: 5,
            elevation: 1,
            borderRadius: 3,
            borderColor: '#fff',
            padding: 2,
            backgroundColor: '#fff',
            // margin: 10,
          }}>
          <View style={{}}>
            <Text style={{fontSize: 16, color: '#000'}}>Full_Name:</Text>
            <Text style={{fontSize: 16, color: '#000'}}>Email:</Text>
            <Text style={{fontSize: 16, color: '#000'}}>Title:</Text>
            {/* <Text style={{fontSize: 16, color: '#000'}}>Link:</Text> */}
          </View>
          <View style={{marginLeft: 6}}>
            <Text style={{fontSize: 16, fontWeight: '400'}}>
              {item.FullName}
            </Text>

            <Text style={{fontSize: 14, fontWeight: '300'}}>{item.Email}</Text>
            <Text style={{fontSize: 14, fontWeight: '400'}}>
              {_detailregisterevent?.Title}
            </Text>
            <TouchableOpacity
              onPress={() => onFinish(item, _detailregisterevent?.Link)}>
              <Text style={{fontSize: 14, fontWeight: '500'}}>
                {_detailregisterevent?.Link}
              </Text>
            </TouchableOpacity>
            {/* {userStore?.user?.role === 'admin' && ()} */}
          </View>
          <View>
            {userStore?.user?.role === 'admin' && (
              <TouchableOpacity
                style={{
                  backgroundColor: '#FF0000',
                  width: 28,
                  height: 28,
                  borderRadius: 9999,
                  position: 'absolute',
                  right: -4,
                  bottom: -8,
                  justifyContent: 'center',
                  alignItems: 'center',
                  // margin: 10,
                  marginBottom: 70,
                  marginRight: 80,
                }}
                onPress={() => onDelete(item)}>
                <AwesomeIcom name="trash-o" size={15} color="#fff" />
              </TouchableOpacity>
            )}
          </View>
        </View>
      </ScrollView>
    );
  };
  return (
    <View>
      {/* header */}
      <View
        style={{
          // flex: 1,
          padding: 10,
          backgroundColor: '#6AB7E3',
          flexDirection: 'row',
          justifyContent: 'center',
          alignContent: 'center',
          // marginBottom: 30,
        }}>
        <Text style={{fontSize: 25, color: 'white'}}>REGISTRATION LIST</Text>
      </View>
      {/* body */}
      <View>
        <View>
          <TextInput
            placeholder="Sreach here..."
            value={query}
            onChangeText={text => onSearch(text)}
          />
        </View>
        <FlatList
          data={data}
          //   keyExtractor={item => {
          //     item.id.toString();
          //   }}
          renderItem={renderItem}
        />
      </View>
    </View>
  );
};

export default ListRegister;
