import React from 'react';
// import {View, Text, Button, TouchableOpacity} from 'react-native';
import AppLogin from '../Login/Login';
import HomeScreen from '../Home/Home';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
// import {createDrawerNavigator} from '@react-navigation/drawer';
// import AwesomeIcom from 'react-native-vector-icons/FontAwesome';
// import {useNavigation} from '@react-navigation/native';
// import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import AppSignup from '../Signup/Signup';
import Stores, {StoreProvider, userStore} from '../../store/index';
import firestore from '@react-native-firebase/firestore';
import DetailRegisterevent from '../Home/DetailRegisterevent';
import {GoogleSignin} from '@react-native-google-signin/google-signin';
import Registerevent from '../Home/Registerevent';
import Settings from '../Home/Settings';
import ListRegister from '../List/Register';
import TabScreen from '../Home/TabScreen';
import Register from '../List/Register';
import Chatbot from '../Home/Chatbot';
import DetailEvent from '../Home/DetailEvent';
import Charts from '../List/Charts';
import {createDrawerNavigator} from '@react-navigation/drawer';

const Stack = createNativeStackNavigator();

GoogleSignin.configure({
  webClientId:
    '242217580355-2lad1u1v17lj7gm1mn908tadugaudpke.apps.googleusercontent.com',
});

const stores = {
  userStore,
  // Add more stores if needed
};

const Mainscreen = () => {
  // const navigation = useNavigation();
  return (
    <StoreProvider store={Stores}>
      <NavigationContainer>
        <Stack.Navigator screenOptions={{headerShown: false}}>
          <Stack.Screen name="Login" component={AppLogin}></Stack.Screen>
          <Stack.Screen name="Signup" component={AppSignup}></Stack.Screen>
          <Stack.Screen name="Home" component={HomeScreen}></Stack.Screen>
          <Stack.Screen
            name="ListRegister"
            component={ListRegister}></Stack.Screen>
          <Stack.Screen
            name="Registerevent"
            component={Registerevent}></Stack.Screen>
          <Stack.Screen
            name="DetailRegisterevent"
            component={DetailRegisterevent}></Stack.Screen>
          <Stack.Screen
            name="DetailEvent"
            component={DetailEvent}></Stack.Screen>
          <Stack.Screen name="Charts" component={Charts}></Stack.Screen>
          <Stack.Screen name="Chatbot" component={Chatbot}></Stack.Screen>
        </Stack.Navigator>
      </NavigationContainer>
    </StoreProvider>
    // <TabScreen />
    // <Registerevent />
    // <Settings />
    // <ListYear />
    // <ListMajors />
    // <Register />
    // <Chatbot />
    // <DetailRegisterevent />
    // <CommentInput />
  );
};
export default Mainscreen;
