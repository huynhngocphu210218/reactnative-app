import {
  View,
  Text,
  ScrollView,
  TextInput,
  Button,
  TouchableOpacity,
  Alert,
  Modal,
  FlatList,
  Image,
} from 'react-native';
import {Paragraph} from 'react-native-paper';
import React from 'react';
import {Dropdown} from 'react-native-element-dropdown';
import {useState, useEffect} from 'react';
import {getAllDocsFromCollection} from '../../utils/Common';
import firestore from '@react-native-firebase/firestore';
import AwesomeIcom from 'react-native-vector-icons/FontAwesome';
import Title from './Title/Subtitle/Title';
import {useRoute} from '@react-navigation/native';
import Subtitle from './Title/Subtitle/Subtitle';
import storage from '@react-native-firebase/storage';
import {useStore} from '../../store/index';
import AweSomeIcon from 'react-native-vector-icons/FontAwesome';
import auth from '@react-native-firebase/auth';
// import firestore from '@react-native-firebase/firestore';

const DetailEvent = () => {
  const [data, setData] = useState([]);
  const [datauser, setDataUser] = useState([]);
  const route = useRoute();
  const {userStore} = useStore();
  console.log('kkk userStore', userStore?.user);
  const myParamDetail: any = route.params;
  console.log('kkkkk myParamDetail', myParamDetail);
  // const [text, setText] = useState('');
  const [comment, setComment] = useState('');
  const [comments, setComments] = useState([]);
  const [item, setItem] = useState<any>({});
  const [datacomment, setDataComment] = useState([]);
  const userId = auth().currentUser.uid;

  const formatDate = timestamp => {
    const date = new Date(timestamp.seconds * 1000);
    const options = {
      year: 'numeric',
      month: 'numeric',
      day: 'numeric',
      hour: 'numeric',
      minute: 'numeric',
    };
    return new Intl.DateTimeFormat('en-US', options).format(date);
  };

  useEffect(() => {
    // Fetch existing comments from Firestore
    const unsubscribe = firestore()
      .collection('comments')
      .where('userId', '!=', userId && 'userId', '==', userId)
      .onSnapshot(snapshot => {
        const commentsArray = [];
        snapshot.forEach(doc => {
          commentsArray.push({id: doc.id, ...doc.data()});
        });
        setComments(
          commentsArray.filter((k: any) => k._id === myParamDetail.id),
        );
      });

    // Clean up the subscription when the component unmounts
    return () => unsubscribe();
  }, [userId]);

  const handleAddComment = async () => {
    const isDuplicate = comments.some(cmd => cmd.text === comment);
    if (comment.trim() !== '' && !isDuplicate) {
      await firestore().collection('comments').add({
        text: comment,
        timestamp: firestore.FieldValue.serverTimestamp(),
        _id: item,
        userId,
      });
      setComment(''); // Clear the input field after adding the comment
    } else {
      Alert.alert('Warning', 'Duplicate comment detected', [
        {text: 'OK', onPress: () => console.log('OK Pressed')},
      ]);
    }
  };
  useEffect(() => {
    getData();
  }, []);

  const getData = async () => {
    const _event = await getAllDocsFromCollection('Event');
    for (let index = 0; index < _event.length; index++) {
      const _ev = _event[index];
      if (_ev.FileName) {
        const url = await storage().ref(_ev.FileName).getDownloadURL();
        _event[index]['Uri'] = url;
      }
    }
    const _user = await getAllDocsFromCollection('Users');
    setDataUser(_user);
    console.log('kkk table _user', _user);
    // const _commnent = await getAllDocsFromCollection('comments');
    // setDataComment(_commnent);
    setData(_event.filter((k: any) => k.id === myParamDetail.id));
    setItem(myParamDetail.id);
    console.log('kkk myparamdetailid', myParamDetail.id);
    console.log('kkk table _event', _event);
    // console.log('kkk _commnent', _commnent);
  };
  const onDelete = _item => {
    firestore()
      .collection('Event')
      .doc(_item.id)
      .delete()
      .then(() => {
        console.log('Event deleted!');
      });
    // setModalDelete(false);
  };
  const renderCommentItem = ({item}) => {
    const _usersid = datauser.filter((k: any) => k.id === item.userId);
    console.log('kkk _usersid', _usersid);
    const timestamp =
      item.timestamp && item.timestamp.seconds
        ? new Date(item.timestamp.seconds * 1000) // Convert seconds to milliseconds
        : null;
    return (
      <View>
        <View
          style={{
            margin: 4,
            flexDirection: 'row',
          }}>
          <View
            style={{
              backgroundColor: 'gray',
              width: 28,
              height: 28,
              borderRadius: 9999,
              // position: 'absolute',
              right: 4,
              bottom: -8,
              justifyContent: 'center',
              alignItems: 'center',
              // margin: 10,
              marginBottom: 10,
              // marginRight: 10,
            }}></View>
          {/* <Text>{item.userId}</Text> */}
          {/* <Text>{_usersid.Name}</Text> */}
          {/* <Text>{item.id}</Text> */}
          <Text
            style={{
              fontWeight: '400',
              fontSize: 16,
              backgroundColor:
                item.userId === 'pXm16iufY6QjtGjMMjz9ZpI1y473'
                  ? '#00FFFF'
                  : '#007bff',
              borderRadius: 5,
              padding: 5,
              right: 1,
              justifyContent: 'center',
              alignSelf:
                item.userID === 'pXm16iufY6QjtGjMMjz9ZpI1y473'
                  ? 'flex-end'
                  : 'flex-start',
              marginRight: 25,
            }}>
            {item.text}
          </Text>
        </View>
        {timestamp && (
          <Text
            style={{
              color: 'gray',
            }}>{`${timestamp.toLocaleString()}`}</Text>
        )}
      </View>
    );
  };
  const renderItem = ({item}) => {
    return (
      <ScrollView>
        <View
          style={{
            marginHorizontal: 1,
            marginVertical: 5,
            // elevation: 1,
            borderRadius: 10,
            borderColor: 'black',
            // padding: 10,
            // backgroundColor: '#fff',
            // borderWidth: 0.5,
          }}>
          <View style={{padding: 5}}>
            <View>
              {item.Uri && (
                <Image
                  source={{uri: item.Uri}}
                  style={{height: 200, width: 'auto', borderRadius: 15}}
                />
              )}
              <Text style={{fontSize: 18, fontWeight: 'bold', margin: 15}}>
                {item.Title}
              </Text>
              <Text style={{fontSize: 16, fontWeight: '400'}}>
                {item.Drescription}
              </Text>
              <Text
                style={{
                  alignContent: 'center',
                  fontSize: 20,
                  // margin: 10,
                  fontWeight: 'bold',
                  borderBottomWidth: 1,
                }}>
                Đặt câu hỏi và trả lời ???
              </Text>

              <FlatList
                data={comments}
                renderItem={renderCommentItem}
                keyExtractor={item => item.id}
              />
              <View
                style={{
                  flexDirection: 'row',
                  // justifyContent: 'center',
                  // alignItems: 'center',
                }}>
                <View
                  style={{
                    // marginTop: 150,
                    // flex: 2,
                    backgroundColor: '#E8E8E8',
                    borderRadius: 15,
                    borderWidth: 1,
                    flexDirection: 'row',
                    width: '80%',
                    // justifyContent: 'center',
                    // alignItems: 'center',
                    // marginBottom: 15,
                    // marginLeft: 10,
                  }}>
                  <View>
                    <TextInput
                      style={{fontSize: 18}}
                      placeholder={'Add a comment...'}
                      multiline={true}
                      value={comment}
                      onChangeText={text => setComment(text)}
                    />
                  </View>
                </View>
                <View>
                  <TouchableOpacity
                    style={{margin: 10}}
                    onPress={handleAddComment}>
                    <AweSomeIcon
                      style={{marginBottom: 5}}
                      name="send"
                      size={20}
                      color={'#0000FF'}
                    />
                  </TouchableOpacity>
                </View>
              </View>

              <View>
                {userStore?.user?.role === 'admin' && (
                  <TouchableOpacity
                    style={{
                      backgroundColor: '#FF0000',
                      width: 28,
                      height: 28,
                      borderRadius: 9999,
                      position: 'absolute',
                      right: -4,
                      bottom: -8,
                      justifyContent: 'center',
                      alignItems: 'center',
                      // margin: 10,
                      // marginBottom: 120,
                      marginRight: 5,
                    }}
                    onPress={() => onDelete(item)}>
                    <AwesomeIcom name="trash-o" size={15} color="#fff" />
                  </TouchableOpacity>
                )}
              </View>
            </View>
          </View>
        </View>
      </ScrollView>
    );
  };
  return (
    <View>
      <FlatList
        data={data}
        //   keyExtractor={item => {
        //     item.id.toString();
        //   }}
        renderItem={renderItem}
      />
    </View>
  );
};

export default DetailEvent;
