import Network from './Network';

// Generate key: https://makersuite.google.com/app/apikey
// Guideline: https://developers.generativeai.google/tutorials/curl_quickstart
class GoogleAI {
  constructor() {
    this.token = 'AIzaSyCL6VQNfIWdk-cbebaWI_haBJXigxXj06E';
    this.baseURL = `https://generativelanguage.googleapis.com/v1beta3/models/`;
  }

  async genText(ipText) {
    return await Network.post(
      `${this.baseURL}text-bison-001:generateText?key=${this.token}`,
      {
        prompt: {
          text: ipText,
        },
      },
    );
  }

  async genMessage(ipMsg) {
    return await Network.post(
      `${this.baseURL}chat-bison-001:generateMessage?key=${this.token}`,
      {
        prompt: {messages: [{content: ipMsg}]},
        //   temperature: 0.1,
        //   candidate_count: 1,
        //   topP: 0.8,
        //   topK: 10,
      },
    );
  }

  async embedText(ipText) {
    return await Network.post(
      `${this.baseURL}embedding-gecko-001:embedText?key=${this.token}`,
      {text: ipText},
    );
  }

  async countMessage(ipMsg) {
    return await Network.post(
      `${this.baseURL}chat-bison-001:countMessageTokens?key=${this.token}`,
      {
        prompt: {
          messages: [{content: 'How many tokens?'}, {content: ipMsg}],
        },
      },
    );
  }
}

export default new GoogleAI();
