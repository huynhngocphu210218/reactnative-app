import React from 'react';
import {
  View,
  Text,
  Modal,
  TextBase,
  TouchableOpacity,
  Button,
} from 'react-native';
// import LinearGradient from 'react-native-linear-gradient';

const SysModal = ({message, visible, onHideModal}) => {
  return (
    <Modal visible={visible} transparent={true}>
      <View
        style={{
          flex: 1,
          backgroundColor: 'rgba(00,00,00,.5)',
          justifyContent: 'center',
          alignItems: 'center',
          padding: 20,
        }}>
        <View
          style={{
            width: '100%',
            backgroundColor: 'white',
            borderRadius: 10,
            padding: 20,
          }}>
          {/* header */}
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              marginBottom: 20,
            }}></View>
          {/* body */}
          <View>
            <Text>{message}</Text>
          </View>
          {/* footer */}
          <View
            style={{
              marginTop: 20,
            }}>
            <Button title="close" onPress={onHideModal}></Button>
          </View>
        </View>
      </View>
    </Modal>
  );
};

export default SysModal;
