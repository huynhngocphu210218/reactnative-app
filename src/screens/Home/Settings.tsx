import {View, Text, TouchableOpacity, Button, Image} from 'react-native';
import React, {useContext} from 'react';
import AwesomeIcom from 'react-native-vector-icons/FontAwesome';
import {useNavigation} from '@react-navigation/native';
import {observer} from 'mobx-react';
import {useStore} from '../../store/index';

const Settings = observer(() => {
  const navigation = useNavigation();
  const {userStore} = useStore();
  console.log('kkk userStore', userStore?.user);

  return (
    <View>
      {/* header */}
      <View
        style={{
          // flex: 1,
          padding: 10,
          backgroundColor: '#6AB7E3',
          flexDirection: 'row',
          justifyContent: 'center',
          alignContent: 'center',
        }}>
        <Text style={{fontSize: 25, color: 'white'}}>SETTING</Text>
      </View>
      {/* body */}
      <View style={{margin: 30}}>
        <View
          style={{padding: 24, justifyContent: 'center', alignItems: 'center'}}>
          <View
            style={{
              padding: 24,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <TouchableOpacity>
              <View
                style={{
                  position: 'relative',
                  width: 72,
                  height: 72,
                  borderRadius: 9999,
                  padding: 24,
                  backgroundColor: 'gray',
                }}></View>
              <View
                style={{
                  backgroundColor: '#007bff',
                  width: 28,
                  height: 28,
                  borderRadius: 9999,
                  position: 'absolute',
                  right: -4,
                  bottom: -8,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <AwesomeIcom name="pencil" size={15} color="#fff" />
              </View>
            </TouchableOpacity>
          </View>
          <Text
            style={{
              marginTop: 5,
              fontSize: 19,
              fontWeight: '600',
              color: '#414d63',
              textAlign: 'center',
            }}>
            Huỳnh Ngọc Phú
          </Text>
        </View>
        <View style={{paddingHorizontal: 24}}>
          {userStore?.user?.role === 'admin' && (
            <TouchableOpacity
              style={{
                paddingVertical: 12,
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
                height: 50,
                backgroundColor: '#f2f2f2',
                borderRadius: 8,
                marginBottom: 12,
                paddingHorizontal: 12,
              }}
              onPress={() => navigation.navigate('Charts')}>
              <View
                style={{
                  width: 32,
                  height: 32,
                  borderRadius: 999,
                  marginRight: 12,
                  justifyContent: 'center',
                  alignItems: 'center',
                  backgroundColor: 'white',
                }}>
                <AwesomeIcom name="bar-chart" size={18} />
              </View>
              <Text
                style={{
                  fontSize: 12,
                  fontWeight: '600',
                  color: 'black',
                  textTransform: 'uppercase',
                  letterSpacing: 1.1,
                }}>
                STATISTICAL
              </Text>
            </TouchableOpacity>
          )}

          <TouchableOpacity
            style={{
              paddingVertical: 12,
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center',
              height: 50,
              backgroundColor: '#f2f2f2',
              borderRadius: 8,
              marginBottom: 12,
              paddingHorizontal: 12,
            }}
            onPress={() => navigation.navigate('ListRegister')}>
            <View
              style={{
                width: 32,
                height: 32,
                borderRadius: 999,
                marginRight: 12,
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: 'white',
              }}>
              <AwesomeIcom name="table" size={18} />
            </View>
            <Text
              style={{
                fontSize: 12,
                fontWeight: '600',
                color: 'black',
                textTransform: 'uppercase',
                letterSpacing: 1.1,
              }}>
              REGISTRATION LIST
            </Text>
          </TouchableOpacity>
          {/* {userStore?.user?.role === 'admin' && ( )} */}
        </View>
      </View>
      {/* footer   */}
      <View style={{justifyContent: 'center', alignItems: 'center'}}>
        <TouchableOpacity
          style={{
            width: 40,
            height: 40,
            padding: 10,
            backgroundColor: '#DB4437',
            borderRadius: 50,
            justifyContent: 'center',
            alignItems: 'center',
            marginLeft: 15,
          }}
          onPress={() => navigation.navigate('Login')}>
          <AwesomeIcom name="power-off" size={20} color={'white'} />
        </TouchableOpacity>
      </View>
    </View>
  );
});

export default Settings;
